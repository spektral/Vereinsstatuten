STATUTEN DES VEREINS freefutureforces

Free Future Forces - Verein zur Vernetzung und Förderung von 
Menschen und Projekten im Bereich Ökologie und sozialem 
Engagement

1 Name, Sitz und Tätigkeitsbereich

1. Der Verein führt den Namen „freefutureforces - Kulturverein 
  zur Verbindung und Vernetzung selbstorganisierter Projekte mit 
  Befreiungscharakter"

2. Er hat seinen Sitz in Graz und erstreckt seine Tätigkeit auf 
  Graz und Umgebung, aber auch Österreich und Europa.

3. Die Errichtung von Zweigvereinen ist möglich.

2 Zweck 

Der Verein, dessen Tätigkeit nicht auf Gewinn gerichtet ist, hat 
folgende Ziele :

• Der Verein bildet ein Forum bzw. eine Plattform, deren Ziel die 
  Vermittlung und Vernetzung von Menschen und deren Ressourcen 
  ist. Durch die Auseinandersetzung mit Kunst und Kultur soll die 
  Entwicklung und Entfaltung des kreativen schöpferischen 
  Potentials im Hinblick auf eine alternative Freizeitgestaltung, 
  Lebensführung und Lebensraumgestaltung ermöglicht werden.

• Kommunikation und Zusammenleben verschiedener Menschen soll 
  angeregt und somit ein sensibleres Bewusstsein für Umwelt und 
  Umfeld (Lebensraum) geschaffen werden.

• Prinzipielle Unterstützung und Förderung von Ideen, die an den 
  Verein herangetragen werden und deren Inhalte dem Vorstand 
  nicht bedenklich erscheinen.

• "freefutureforces" will Impulse setzen. Kernaufgabe ist es, 
  Verbindungen, Vernetzung und Zusammenarbeit verschiedener 
  Gruppen und Einzelindividuen, die sich mit selbstorganisierten 
  revolutionären Projekten mit Befreiungscharakter beschäftigen 
  zu fördern, um auf lokaler, regionaler, nationaler und 
  internationaler Ebene gemeinsam aktiv zu werden.

• Auf lokaler Ebene sind wir bemüht die vielen Gruppen, Vereine, 
  Aktionsgemeinschaften und aktiven Einzelkünstler einzuladen 
  mehr Kommunikation und Kooperation innerhalb der Grazer Szene 
  zu betreiben. Ohne freien Willen und Unabhängigkeit zu 
  gefährden soll durch Netzwerkarbeit und Lobbying erreicht 
  werden, mehr Möglichkeiten, mehr Kraft, mehr Stimme und mehr 
  Aufmerksamkeit für einzelne Aktivitäten und Projekte zu 
  schaffen.

• Auf internationaler Ebene soll ein Netzwerk von Gleichgesinnten 
  & Geschwisterorganisationen in anderen Ländern geknüpft werden. 
  Dies soll es jungen Menschen erleichtern an anderen Orten 
  Anschluss & Freunde zu finden und dadurch ein ganz anderes, 
  authentischeres und unmittelbares Eintauchen in die jeweilige 
  Urbanstruktur zu ermöglichen. In weiterer Folge soll es dadurch 
  jungen sowie unbekannten Künstlern und Aktionsgruppen 
  ermöglicht werden ihren Schaffensradius zu vergrößern, 
  Erfahrungen zu sammeln und einen ersten Hauch von 
  Internationalität mitzubekommen. 

• Die Erweiterung des Erfahrungshorizont, wechselseitiges 
  Kennenlernen und die Erforschung anderer Realitäten und 
  Lebensumstände sowie der Art und Weise der Zusammenarbeit und 
  Organisation sind beispielhafte Ziele die hierbei verfolgt 
  werden. Im Laufe der Zeit wollen wir bestehende Beziehungen 
  vertiefen, auf überregionaler Ebene mit gleichgesinnten Gruppen 
  in Kontakt treten und auf internationalem Terrain gute Partner 
  für zukünftige Projekte kennenzulernen. Ergebnisse und 
  Erlebnissen sollen zu wechselseitiger Belebung, Anregung und 
  Inspiration führen.

• Es geht darum, die urbanen Räume als wertvolle Gebiete mit 
  interessanter eigenständiger Kultur und Wirtschaft zu 
  begreifen, sie in wechselseitig fruchtbringender Beziehung mit 
  anderen Regionen zu sehen und zu setzen, sich mit bestehenden 
  Realitäten auseinanderzusetzen und sie auf persönlicher, 
  soziokultureller und internationaler Ebene zu analysieren. In 
  spielerischer Manier werden Strategien entdeckt, um gemeinsamin 
  eine positive Richtung weiterzugehen und sich auf 
  hoffnungsspendende und nachhaltige Weise weiterzuentwickeln. 
  Längerfristig wird angestrebt, dass sich auf lokaler und 
  internationaler Ebene gemeinsame übergreifende Aktivitäten 
  bilden, eine Entwicklung auf individueller und kollektiver 
  Ebene gefördert wird und damit grössere Gemeinschaftsprojekte 
  realisiert werden. Die Auswirkungen aller Unternehmungen von 
  "freefutureforces" werden sichtbar indem sich unterschiedliche 
  „Communities“ in regem Austausch in ihrer Arbeitsweise, 
  Kreativität und Umsetzung inspirieren und unterstützen. 
  Besseres Verständnis und wechselseitiger Wissens- und 
  Erfahrungsaustausch bringen eine Förderung des interkulturellen 
  Dialogs im Sinne der Kooperation und Kommunikation 
  unterschiedlicher Lebensräume und Kulturszenen.

3 Mittel zur Erreichung des Vereinszwecks

1. Der Vereinszweck soll durch die in den Abs. 2 und 3 
  angeführten ideellen und materiellen Mittel erreicht werden.

2. Als ideelle Mittel zur Erreichung des Vereinszwecks dienen:

  (a) Organisation von Lesungen, Vorträgen, Aufführungen (Theater 
    und Kleinkunst), Tausch- und Flohmärkten, Ausstellungen, 
    kulinarischer Austausch und Küchenkünste, 
    Reiseunternehmungen, Konzerten, Musik und Videoevents, 
    Tanzveranstaltungen in deren Rahmen Künstler*innen, 
    Interessierten und die Möglichkeit zu Interaktion und 
    konstruktivem Austausch geboten werden soll.

  (b) Die Organisation von Projekten, die auf internationaler 
    grenzenüberschreitender Ebene Interaktion und Austausch 
    anregen und Netzwerkarbeit vorantreiben: Jugendbegegnungen, 
    Seminare, Workshops, Versammlungen, Organisationstreffen, 
    Kongresse, Tagungen, Netzwerkprojekte, Freiwilligendienst, 
    Austauschmaßnahmen.

  (c) Die Bereitstellung von Räumlichkeiten und entsprechendem 
    Equipment (z.B. Proberäume inklusive Einrichtung, Medienlabor 
    mit Computer, ...), um die Entfaltungsmöglichkeiten und 
    Arbeitsweisen auf verschiedenen künstlerischen sowie 
    kulturellen Gebieten zu vergrößern als auch den Zugang zu 
    Information und Ausrüstung zu erleichtern. Hilfestellung bei 
    Transporten, sowie Auf- und Abbauten.

  (d) Gegenseitige Unterstützung von Mitgliedern, 
    Partnercommunities und gleichgesinnten Vereinen bei der 
    Verwirklichung eigenständiger Projekte.

  (e) Organisation von Seminaren und Workshops um Wissen und 
    Fähigkeiten in verschiedenen soziokulturellen und 
    künstlerischen Bereichen zu vermitteln.

3. Die erforderlichen materiellen Mittel werden aufgebracht 
  durch:

  (a) Beitrittsgebühren und Mitgliedsbeiträge

  (b) Erträge aus Veranstaltungen, vereinseigenen Unternehmungen

  (c) Leihgebühren, Mieteinnahmen und Unkostenbeiträge

  (d) Förderungen, Subventionen, Sponsoring

  (e) Spenden, Sammlungen, Vermächtnisse 

  (f) Zuwendungen

4 Mitglieder

1. Mitglieder des Vereins können alle physischen Personen sowie 
  juristische Personen und rechtsfähige Personengesellschaften 
  werden, die keine rassistischen, sexistischen, 
  diskriminierenden oder umweltzerstörerischen Absichten oder 
  Praktiken verfolgen.

2. Die Mitglieder des Vereins gliedern sich in ordentliche, 
  assoziierte und Fördermitglieder.

3. Ordentliche Mitglieder sind jene, die sich ehrenamtlich voll 
  an der Vereinsarbeit im Sinne der Statuten beteiligen, sowie 
  als solche über Antrag vom Vorstand aufgenommen werden. 
  Ordentliche Mitglieder sind berechtigt, an der 
  Generalversammlung teilzunehmen, das aktive und passive 
  Wahlrecht auszuüben, und Funktionen im Verein zu bekleiden. Das 
  aktive Wahlrecht ermöglicht es einem, zu wählen, das passive 
  gewählt zu werden, also zu kandidieren. Ordentliche Mitglieder 
  verpflichten sich zur Zahlung eines vom Plenum festzusetzenden 
  Mitgliedsbeitrages, sofern dieser in der Vereinspraxis 
  beschlossen wird. 

  (a) Ordentliches Mitglied des Vereins „freefutureforces“ kann 
    weiters jede gemeinnützige und mildtätige juristische Person 
    werden, die einige oder alle der unter § 2 genannten Aufgaben 
    zum Ziel unterstützt. 

  (b) Die Anerkennung der Satzung und die Einhaltung der 
    Grundsätze ist jedenfalls Grundlage einer Mitgliedschaft. 

  (c) Die Einschätzung der Einhaltung der Grundsätze des 
    Antragstellers obliegt dem Verein „freefutureforces“. 

  (d) Jedes Mitglied kann hier einen begründeten schriftlichen 
    Antrag auf Verwehrung der Aufnahme einbringen. Dieser ist in 
    der Entscheidung zu berücksichtigen. 

  (e) Ein Rechtsanspruch auf Aufnahme besteht nicht. Eine 
    Berufung ist daher nicht zulässig. Ablehnungen müssen 
    begründet sein.

4. Assoziierte Mitglieder können an allen Veranstaltungen des 
  Vereins teilnehmen und haben bei Mitgliederversammlungen ein 
  Anhörungsrecht. Sie verfügen jedoch weder über ein aktives noch 
  ein passives Stimmrecht. Assoziierte Mitglieder des Vereins 
  unterstützen den Verein nach gegebenen Anlass freiwillig, aber 
  sind dazu nicht verpflichtet. 

5. Fördermitglieder unterstützen den Verein finanziell, ohne dazu 
  verpflichtet zu sein, sich an anderen Tätigkeiten zu 
  beteiligen. Fördermitgliedern erwächst durch ihre regelmäßige 
  Unterstützung des Vereins keinerlei Rechtsanspruch.

5 Organe und Instrumente des Vereins

1. Organe des Vereins sind die Generalversammlung, das 
  Koordinationsteam, das Plenum, die Rechnungsprüfer*innen und 
  das Schiedsgericht. (Siehe § 9, 10, 11)

2. Die Vereinspraxis besteht aus Plenumsentscheidungen und wird 
  in Form schriftlicher Protokolle festgehalten.

6 Konsensentscheidungen

Soweit in diesem Statut Konsensentscheidungen vorgesehen sind, 
erfolgen diese nach folgendem Verfahren:

1. Konsens bedeutet, dass nach eindeutiger und klarer 
  Formulierung eines Entscheidungsvorschlages keine der 
  anwesenden stimmberechtigten Personen ausdrücklich Einwände 
  erhebt. In diesem Fall gilt der Vorschlag als angenommen und 
  wird im Protokoll vermerkt.

2. Bei Einwänden müssen diese begründet und diskutiert werden. 
  Daraufhin wird ein neuer Entscheidungsvorschlag formuliert, in 
  den die Ergebnisse dieser Diskussion einfließen, woraufhin 
  nochmals nach Konsens gefragt wird.

3. Kann kein Konsens gefunden werden stehen 2 Möglichkeiten 
  offen:

  (a) Ist die Entscheidung dringend, kann im Konsens eine 
    sofortige Abstimmung über den letzten Entscheidungsvorschlag 
    beschlossen werden. Es gilt Zweidrittelmehrheit.

  (b) Ist die Entscheidung nicht dringend, kann im Konsens eine 
    Vertagung beschlossen werden.

4. Wenn einzelne Personen zwar Bedenken gegenüber einer 
  bestimmten Entscheidung hegen, die Beschlussfassung aber nicht 
  behindern wollen, besteht die Möglichkeit, diese Bedenken zu 
  Protokoll zu geben, ohne dass die Entscheidung dadurch 
  beeinträchtigt wird. 

7 Generalversammlung

1. Die ordentliche Generalversammlung wird mindestens einmal im 
  Jahr einberufen. Allerdings sofort, wenn das gesamte 
  Koordinationsteam den Antrag zum Rücktritt stellt.

2. Die Generalversammlung ist beschlussfähig, wenn alle 
  ordentlichen Mitglieder ordnungsgemäß zwei Wochen vor der 
  Generalversammlung via Brief oder Email eingeladen wurden.

3. Eine außerordentliche Generalversammlung kann einberufen 
  werden durch:

  (a) das Koordinationsteam

  (b) das Plenum

  (c) den oder die Rechnungsprüfer*innen

  (d) wenn zehn Prozent der ordentlichen Mitglieder dies vom 
    Koordinationsteam schriftlich einfordern.

    im Falle von d) muss das Koordinationsteam die 
    Generalversammlung innerhalb eines Monats einberufen.

4. Die Generalversammlung hat das alleinige Recht den Verein mit 
  Zweidrittelmehrheit aufzulösen.

5. Die Generalversammlung entscheidet im Konsens. 

7.1 Die Generalversammlung hat folgende Aufgaben und Rechte:

1. Beschlussfassung über den Voranschlag.

  • Der Voranschlag listet die Zahlungen und kalkulatorischen 
    Positionen auf, die für die folgende Planungsperiode erwartet 
    werden.

  • Meist geht der angenommene Bedarf über die maximal zur 
    Verfügung stehenden Finanzmittel hinaus.

2. Entgegennahme und Genehmigung des Rechenschaftsberichts und 
  des Rechnungsabschlusses unter Einbindung der 
  Rechnungsprüfer*innen.

3. Genehmigung von Rechtsgeschäften zwischen 
  Rechnungsprüfer*innen und Verein.

4. Entlastung des Koordinationsteams.

5. Festsetzung der Höhe der Beitrittsgebür und der 
  Mitgleidsbeiträge, so vorhanden, für ordentliche und für 
  assozierte Mitglieder.

6. Der Generalversammlung ist die Änderung der Statuten 
  vorbehalten.

7. Beratung und Beschlussfassung über sonstige auf der 
  Tagesordnung stehende Fragen.

8. Die Generalversammlung wählt das Koordinationsteam und neue 
  Mitglieder des Koordinationsteams im Konsens.

9. Sie hat das Recht, das Koordinationsteam oder einzelne 
  Mitglieder des Koordinationsteams ihres Amtes zu entheben, 
  wobei die betreffenden Personen nicht stimmberechtigt sind. Die 
  Enthebung tritt mit Bestellung des neuen Koordinationsteams 
  bzw. Mitglieds in Kraft.

8 Koordinationsteam

1. Das Koordinationsteam ist das Leitungsorgan des Vereins im 
  Sinne VerG 02.

2. Die Funktionsperiode des Koordinationsteams beträgt ein Jahr.

3. Das Koordinationsteam setzt sich aus mindestens drei 
  natürlichen Personen zusammen, die gleichzeitig ordentliche 
  Mitglieder des Vereins sein müssen.

4. Das Koordinationsteam ist ein Team von gleichberechtigten 
  Personen, die die Aufgaben der Repräsentation nach Außen, der 
  Finanzverantwortung und des Schriftverkehrs abdecken.

5. Dem Koordinationsteam obliegen die operative Leitung und die 
  Geschäftsführung des Vereins.

6. Die Tätigkeit des Koordinationsteams kann durch Beschlüsse des 
  Plenums, insbesondere durch die Vereinspraxis oder 
  Vetoentscheide, weiter eingeschränkt beziehungsweise definiert 
  werden.

7. Das Koordinationsteam ist beschlussfähig, wenn alle Mitglieder 
  des Koordinationsteams eingeladen wurden und mindestens die 
  Hälfte anwesend ist. 

  • Besteht der Vorstand nur aus drei Mitgliedern, ist er dann 
    beschlussfähig, wenn 2 Mitglieder des Koordinationsteams 
    anwesend sind. 

  • Weiters gelten im Konsens getroffene Plenumsbeschlüsse - 
    soweit erforderlich auch als Beschlüsse des 
    Koordinationsteams, sofern mindestens die Hälfte der 
    Mitglieder des Koordinationsteams anwesend ist.

8. Jedes ordentliche Mitglied ist berechtigt als Kandidat*innen 
  für das Koordinationsteam vorgeschlagen zu werden, oder sich 
  selbst vorzuschlagen.

9. Über die Aufnahme von Kandidat*innen ins Koordinationsteam im 
  laufenden Geschäftsjahr entscheidet das Plenum. Die 
  Generalversammlung bestätigt oder wählt das Koordinationsteam 
  neu.

10. Das Koordinationsteam trifft Entscheidungen im Konsens.

8.1 Das Koordinationsteam hat folgende Aufgaben und Rechte: 

1. Das Koordinationsteam besitzt das Recht das Plenum und die 
  Generalversammlung einzuberufen. 

2. Schriftliche Ausfertigungen des Vereins bedürfen zu ihrer 
  Gültigkeit der Unterschriften von zwei Mitgliedern des 
  Koordinationsteams. 

3. Rechtsgeschäftliche Bevollmächtigungen, den Verein nach außen 
  zu vertreten bzw. für ihn zu zeichnen, können ausschließlich 
  auf Beschluss des Koordinationsteams erteilt werden. Dieser 
  Beschluss bedarf weiters der ausdrücklichen Zustimmung des 
  Plenums.

4. Das Koordinationsteam ist dafür verantwortlich, dass 
  Protokolle der Generalversammlung, des Plenums und Treffen des 
  Koordinationsteams ordnungsgemäß geführt wird und am besten 
  sofort, sonst innerhalb einer Frist von 3 Tagen, an alle 
  ordentlichen Mitglieder ausgeschickt wird.

5. Die Mitglieder des Koordinationsteams sind zu ungeteilter Hand 
  für die ordnungsgemäße Finanzgebarung des Vereins 
  verantwortlich. 

  (a) Zu geteilter Hand bedeutet hier gemeinschaftliche Schuld, 
    es trägt also das ganze Koordinationteam die ordnungsgemäße 
    Finanzgebarung. 

6. Außer durch den Tod und Ablauf der Funktionsperiode erlischt 
  die Funktion eines Mitglieds des Koordinationsteams durch 
  Enthebung und Rücktritt.

7. Die Mitglieder des Koordinationsteams können jederzeit 
  schriftlich ihren Rücktritt erklären. 

  (a) Die Rücktrittserklärung ist an das Koordinationsteam, im 
    Falle des Rücktritts des gesamten Koordinationsteams an die 
    Generalversammlung zu richten. 

  (b) Der Rücktritt wird erst mit Wahl eines Nachfolgers wirksam, 
    sofern die Mindestanzahl der Mitglieder des Koodinationsteams 
    nicht unterschritten wird, sonst sofort nach Kenntnisname des 
    Plenums.

8. Über die Aufnahme von Kandidat*innen ins Koordinationsteam im 
  laufenden Geschäftsjahr entscheidet das Plenum. Die 
  nachträgliche Genehmigung ist in der nächstfolgenden 
  Generalversammlung einzuholen. 

  (a) Fällt das Koordinationsteam ohne Selbstergänzung durch 
    Beschluss des Plenums überhaupt oder auf unvorhersehbar lange 
    Zeit aus, so ist jeder der Rechnungsprüfer*innen 
    verpflichtet, unverzüglich eine außerordentliche 
    Generalversammlung zum Zweck der Neuwahl eines 
    Koordinationsteams einzuberufen. 

  (b) Sollten auch die Rechnungsprüfer*innen handlungsunfähig 
    sein, hat jedes ordentliche Mitglied, das die Notsituation 
    erkennt, unverzüglich die Bestellung eines Kurators beim 
    zuständigen Gericht zu beantragen, der umgehend eine 
    außerordentliche Generalversammlung einzuberufen hat.

9 Plenum

1. Zur Teilnahme am Plenum sind alle Mitglieder (bei juristischen 
  Personen ein(e) Vertreter*innen) sowie Interessierten (diese 
  ohne ausdrückliches Anhörungsrecht) berechtigt.

2. Das Stimmrecht ist den ordentlichen Mitgliedern vorbehalten.

3. Bei Abstimmungen hat jedes Mitglied eine Stimme. 
  Stimmübertragungen von natürlichen Personen sind nicht 
  zulässig.

4. Das Plenum ist das oberste Gremium des Vereins zwischen den 
  Sitzungen der Generalversammlung.

5. Plena finden regelmäßig, mindestens aber einmal im Monat 
  statt.

6. Die Einberufung bedarf keiner besonderen Form und erfolgt in 
  der Regel automatisch zu einem in der Vereinspraxis 
  festgelegten Termin / Wochentag.

7. Das Plenum ist beschlussfähig,

  (a) dann, wenn entweder mindestens 5 ordentliche Mitglieder,

  (b) sonst, wenn der Verein weniger als 10 ordentliche 
    Mitglieder zum Zeitpunkt des Plenums hat, mind. 50% der 
    ordentliche Mitglieder anwesend sind.

    • Mindestens eine der anwesenden Personen muss dem 
      Koordinationsteam angehören.

    • Sind weniger als 5 ordentliche Mitglieder (im Fall §13 Abs. 
      7a) bzw. 50% (im Fall §13 Abs.7b) anwesend, kann das Plenum 
      trotzdem Beschlüsse treffen. Diese müssen per Mail an alle 
      Mitglieder ausgeschickt werden und jedes Mitglied kann 
      unter Anführung einer Begründung ein Veto gegen den 
      Beschluss einlegen. Beim nächsten Plenum muss unter 
      Einbeziehung der Gegenargumente der Beschluss revidiert 
      werden. Weitere Einsprüche sind dann nicht mehr möglich.

8. Die Entscheidungen des Plenums erfolgen im Konsens.

9.1 Das Plenum hat folgende Aufgaben und Rechte: 

1. Wahl zusätzlicher Mitglieder des Koordinationsteams während 
  der laufenden Funktionsperiode des Koordinationsteams.

2. Das Plenum besitzt ein absolutes Vetorecht bei allen 
  Entscheidungen des Koordinationsteams. 

  • Mitglieder des Koordinationsteams sind bei Vetobeschlüssen 
    nicht stimmberechtigt.

3. Es beauftragt das Koordinationsteam oder andere Personen mit 
  der Erledigung von Arbeiten, insbesondere der Vertretung des 
  Vereins nach außen, und kann diese Bevollmächtigungen jederzeit 
  widerrufen.

4. Das Plenum dient der Koordination der vereinsinternen 
  Arbeitsaufteilung.

5. Es setzt die Mitgliedsbeiträge und Zahlungsmodalitäten fest.

6. Es entscheidet über die Aufnahme sowie den Ausschluss von 
  Mitgliedern . 

  (a) Die Mitgliedschaft endet mit der Auflösung des Vereins 
    (juristischen Person), durch Ausschluss oder Austritt.

  (b) Das Plenum kann über ein Ruhen der Mitgliedschaft 
    entscheiden. 

  (c) Ein Mitglied kann jederzeit in schriftlicher Form gegenüber 
    dem Vorstand austreten. Die Kündigungsfrist beträgt ein 
    Monat. Bereits geleistete Mitgliedsbeiträge werden nicht 
    refundiert.

  (d) Ein Mitglied kann ausgeschlossen werden, wenn es den 
    Vereins-Namen in irreführender Weise trägt oder die 
    Interessen oder Ziele des Vereins verletzt und wegen des 
    gleichen Verstoßes bereits schriftlich abgemahnt wurde. 

    i. Einen Ausschlussantrag kann jedes Mitglied schriftlich 
      beim Plenum stellen. Dieses hat die Begründung des Antrags 
      zu prüfen und die Stellungnahme des beschuldigten Mitglieds 
      einzuholen. 

    ii. Über den Ausschluss von Mitgliedern entscheidet das 
      Plenum. 

    iii. Das Plenum entscheidet über den Ausschluss mit 
      qualifizierter Mehrheit endgültig. 

    iv. Gegen den Ausschluss ist ein Einspruch beim 
      Schiedsgericht des Vereins möglich. Dieses entscheidet 
      endgültig.

7. Das Plenum erlässt und ergänzt die Vereinspraxis, die 
  insbesondere Beschlüsse zu 3.-6. des vorliegenden Absatzes 
  umfasst.

8. Das Plenum kann die Generalversammlung einberufen.

10 Rechnungsprüfer*innen

1. Zwei Rechnungsprüfer*innen werden von der Generalversammlung 
  auf die Dauer von 2 Jahren gewählt. Wiederwahl ist möglich. Die 
  Rechnungsprüfer*innen dürfen keinem Organ - mit Ausnahme der 
  Generalversammlung - angehören, dessen Tätigkeit Gegenstand der 
  Prüfung ist.

2. Den Rechnungsprüfer*innen obliegt die laufende 
  Geschäftskontrolle sowie die Prüfung der Finanzgebarung des 
  Vereins im Hinblick auf die Ordnungsmäßigkeit der 
  Rechnungslegung und die statutengemäße Verwendung der Mittel. 
  Der Koordinationsteam hat den Rechnungsprüfer*innen die 
  erforderlichen Unterlagen vorzulegen und die erforderlichen 
  Auskünfte zu erteilen. Die Rechnungsprüfer*innen haben dem 
  Koordinationsteam über das Ergebnis der Prüfung zu berichten.

3. Rechtsgeschäfte zwischen Rechnungsprüfer*innen und Verein 
  bedürfen der Genehmigung durch die Generalversammlung. Im 
  Übrigen gelten für die Rechnungsprüfer*innen die Bestimmungen 
  des § 7 Abs. 2, 9.1 Abs. 6 und § 8 Abs. 16 bis 18 sinngemäß.

11 Schiedsgericht

1. Zur Schlichtung von allen aus dem Vereinsverhältnis 
  entstehenden Streitigkeiten ist das vereinsinterne 
  Schiedsgericht berufen, dann wenn dies von einer der Parteien 
  gewünscht wird. Es ist eine „Schlichtungseinrichtung“ im Sinne 
  des Vereinsgesetzes 2002 und kein Schiedsgericht nach den §§ 
  577 ff ZPO.

2. Das Schiedsgericht setzt sich aus drei ordentlichen 
  Vereinsmitgliedern zusammen. 

  • Es wird derart gebildet, dass ein Streitteil dem 
    Koordinationsteam ein Mitglied als Schiedsrichter*innen 
    schriftlich namhaft macht. 

    – Über Aufforderung durch den Vorstand, binnen sieben Tagen, 
      macht der andere Streitteil innerhalb von 14 Tagen 
      seinerseits ein Mitglied des Schiedsgerichts namhaft. 

    – Nach Verständigung durch den Vorstand, innerhalb von sieben 
      Tagen, wählen die namhaft gemachten Schiedsrichter binnen 
      weiterer 14 Tage ein drittes ordentliches Mitglied zum/zur 
      Vorsitzenden des Schiedsgerichts. 

    – Bei Stimmengleichheit entscheidet unter den Vorgeschlagenen 
      das Los. Die Mitglieder des Schiedsgerichts dürfen keinem 
      Organ - mit Ausnahme der Generalversammlung - angehören, 
      dessen Tätigkeit Gegenstand der Streitigkeit ist.

3. Das Schiedsgericht fällt seine Entscheidung nach Gewährung 
  beiderseitigen Gehörs bei Anwesenheit aller seiner Mitglieder 
  mit einfacher Stimmenmehrheit. 

  • Es entscheidet nach bestem Wissen und Gewissen. 

  • Seine Entscheidungen sind vereinsintern endgültig.

12 Freiwillige Auflösung des Vereins

1. Die freiwillige Auflösung des Vereins kann nur in einer 
  Generalversammlung und nur mit Zweidrittelmehrheit der 
  abgegebenen gültigen Stimmen beschlossen werden.

2. Diese Generalversammlung hat auch - sofern Vereinsvermögen 
  vorhanden ist - über die Abwicklung zu beschließen. 

  • Insbesondere hat sie eine Abwickler*in oder eine abwickelnde 
    Person zu berufen und Beschluss darüber zu fassen, wem 
    diese*r das nach Abdeckung der Passiven verbleibende 
    Vereinsvermögen zu übertragen hat. 

  • Dieses Vermögen soll, soweit dies möglich und erlaubt ist, 
    einer Organisation zufallen, die gleiche oder ähnliche Zwecke 
    wie dieser Verein verfolgt, sonst Zwecken der Sozialhilfe. 

  • Übertragung des Stimmrechts auf ein anderes Mitglied im Wege 
    einer schriftlichen Bevollmächtigung ist zulässig. 

